const str = 'Alorem5 aipsum dolor5 sit amet5';

const regex = /[a]/gi;

console.log(str.match(regex)); // retourne un tableau avec les éléments correspondant à la regex

console.log(str.replace(regex, '!')); // remplace les éléments correspondant à la regex par !