let myObj = {
    name: 'guihome',
    age: '44',
    taille: '1.80',
    sexe: 'homme',
    presentation: function () {
        console.log(`Bonjour je suis ${this.name} et j'ai ${this.age} ans`);
    }
}

console.log(myObj.presentation());

myObj.hobby = ['coder', 'jouer', 'lire'];

console.log(myObj.hobby);

for( i = 0; i< myObj.hobby.length; i++){
    console.log(myObj.hobby[i]);
}