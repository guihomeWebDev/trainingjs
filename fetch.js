const list = document.querySelector('.list');
const btn = document.querySelector('.btn');

btn.addEventListener('click', () => {

    fetch('https://jsonplaceholder.typicode.com/posts')
        .then(response => response.json())
        .then(data => {

            for(i = 0; i < data.length; i++) {
                let newList = document.createElement('li');
                let newCardTitle = document.createElement('h2');
                let newCardContent = document.createElement('p');

                newCardTitle.innerText = data[i].title;
                newCardContent.innerText = data[i].body;

                newList.appendChild(newCardTitle);
                newList.appendChild(newCardContent);
                list.appendChild(newList);
            }
        })
});