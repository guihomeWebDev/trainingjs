let tab = ['guihome', 'melanie', 'ethan', 'abel'];
let tab2 = ['melinda', 'jessica', 'emma'];
let fruits =['banane', 'pomme', 'poire', 'fraise', 'tomate', 'orange'];
console.log(fruits.join('/'));
console.log(fruits.reverse())
let tab3 = tab.concat(tab2);//concatenation de tableau
console.log(tab3);

console.log(tab3.includes('joseph')); //

tab.push('emmy', 'laurine');
tab.pop('abel');
tab.shift();
tab.unshift('guihome');


for (let i = 0; i < tab.length; i++) {
    console.log(tab[i]);
}

// afficher la derniere valeur d'un tableau

console.log(tab[tab.length - 1]);
console.log(tab.splice(1, 2)); // supprime les 2 valeurs à partir de la position 1
console.log(tab.splice(3, 0, 'laurine', 'emmy')); // ajoute les 2 valeurs à partir de la position 1
console.log(tab.slice(2, tab.length)) //crée un nouveau tableau avec les valeurs à partir de la position 2