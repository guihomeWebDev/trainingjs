const str = 'lorem5 ipsum dolor5 sit amet5';

console.log(str.charAt(4)); // affiche le caractère à la position 4

console.log(str.includes('dolor')); // vérifie si le caractère existe dans la chaine de caractère

console.log(str.indexOf('ipsum')); // retourne la position du caractère dans la chaine de caractère

console.log(str.charCodeAt(4)); // retourne le code ascii du caractère

console.log(str.slice(15));// retourne la chaine de caractère à partir de la position 15
console.log(str.split(' ')); // retourne un tableau avec les éléments séparés par un espace
console.log(str.join(' ')); // retourne une chaine de caractère avec les éléments séparés par un espace

