// loop for in && for
//itéré un objet for in
//itéré un tableau ou string for of


const car = {
    brand: 'bmw',
    model: 'm3',
    color: 'red'
}

for (let key in car) {
    console.log(key, car[key]);
}

fruits = ['banane', 'pomme', 'poire', 'fraise', 'tomate', 'orange'];

for (let fruit of fruits) {
    console.log(fruit, fruits.indexOf(fruit));
}