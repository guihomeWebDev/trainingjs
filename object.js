// const car = {
//     marque : 'Renault',
//     modele : 'Clio',
//     annee : '2015',
//     rouler() {
//         console.log(`Je suis une ${this.marque} ${this.modele} de ${this.annee}`);
//     }
// }
// console.log(object.value(car));
// console.log(object.key(car));

class Car {
    constructor(marque, modele, annee, option) {
        this.marque = marque;
        this.modele = modele;
        this.annee = annee;
        this.option = option;
    }
    addOptions(optionName) {
        this.option++;
        console.log(`Rajout de l'option ${optionName}, nombre d'options : ${this.option}`);
        return this;
    }

    rouler() {
        console.log(`Je suis une ${this.marque} ${this.modele} de ${this.annee}`);
    }
}

voiture = new Car('Renault', 'Clio', '2015', 0);



voiture
.addOptions('climatisation')
.addOptions('vitres éléctriques')
.addOptions('airbag');

class bike extends Car{}

honda = new bike('Honda', 'CBR', '2015', 0);
console.log(honda);
honda.addOptions('chrome echapement');