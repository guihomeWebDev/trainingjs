const numTab = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

numTab.forEach(function (element, index) {
    console.log(element, index);
});

//ou boucle for
for (let i = 0; i < numTab.length; i++) {
    console.log(numTab[i]);
}

const result = numTab.map(function (element) {
    return element * 2;
});
// ou fonction fléché
const result2 = numTab.map(element => (element *2));

console.log(result);
console.log(result2);

const fruits = ['banane', 'pomme', 'poire', 'fraise', 'tomate', 'orange'];

const resultFilter = fruits.filter(function (fruits) {
    return fruits.length > 5;
});

// ou fléché

const resultFilter2 = fruits.filter(fruits => fruits.length > 5);


console.log(resultFilter);
console.log(resultFilter2);

const reduced = numTab.reduce(function (acc, element) {
    return acc + element;
},10);

console.log(reduced);