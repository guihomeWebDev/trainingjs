const number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

//déclaratif
let isPair = [];

for(let i = 0; i < number.length; i++){
    if(number[i] %2 === 0) {
        isPair.push(number[i]);
    }
}

// fonctionnel

const checkIsPair = number => number % 2 === 0;
number.filter(checkIsPair);
console.log(number.filter(checkIsPair));

// expression avec fonction

const carre = n => n * n;
const returnType = n => typeof n;

const handleData = (val, carre, returnType) => {
    let finaleValue;
    finaleValue = carre(val);

    return ` Valeur finale ${finaleValue} est de type ${returnType(val)}`;
}

console.log(handleData(5, carre, returnType));